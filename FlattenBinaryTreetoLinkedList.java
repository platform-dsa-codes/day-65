/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    public void flatten(TreeNode root) {
        if (root == null) return;
        
        // Save the right subtree.
        TreeNode rightSubtree = root.right;
        
        // Flatten the left subtree.
        flatten(root.left);
        
        // Move the flattened left subtree to the right child.
        root.right = root.left;
        
        // Set the left child to null.
        root.left = null;
        
        // Find the rightmost leaf of the original right subtree.
        TreeNode current = root;
        while (current.right != null) {
            current = current.right;
        }
        
        // Append the original right subtree after the flattened left subtree.
        current.right = rightSubtree;
        
        // Continue flattening the original right subtree.
        flatten(rightSubtree);
    }
}
